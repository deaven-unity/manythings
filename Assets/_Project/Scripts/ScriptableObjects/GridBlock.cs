using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "GridBlock", menuName = "Grid/Block", order = 0)]
    public class GridBlock : ScriptableObject
    {
        [PreviewField(75)]
        [HideLabel]
        [AssetList(Path = "_Project/Prefabs/Blocks/", AutoPopulate = true)]
        [AssetsOnly]
        public GameObject prefab;
        public Color color;

        [ShowInInspector]
        public string Hex => ColorUtility.ToHtmlStringRGB(color);

        public int heightLevel;

        public bool destroyable;
    }
}