﻿using System;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Items
{
    [CreateAssetMenu(fileName = "Item", menuName = "Items/BaseItem", order = 0)]
    public class BaseItem : ScriptableObject
    {
        public Sprite img;
        public new String name;
        public int price;
    }
}