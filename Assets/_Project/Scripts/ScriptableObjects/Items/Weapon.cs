﻿using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Items
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Items/Weapon", order = 0)]
    public class Weapon : BaseItem
    {
        public int damage;
    }
}