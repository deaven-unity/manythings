﻿using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "StatsMultiplicationTable", menuName = "StatsMultiplicationTable", order = 0)]
    public class StatsMultiplicationTable : ScriptableObject
    {
        public int strengthMultiply = 3;
        public int intelligenceMultiply = 2;
        public int enduranceMultiply = 5;
        public int dexterityMultiply = 2;
    }
}