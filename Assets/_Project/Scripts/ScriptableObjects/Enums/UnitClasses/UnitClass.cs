﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Enums.UnitClasses
{
    [CreateAssetMenu(fileName = "UnitClass", menuName = "Enums/UnitClass", order = 0)]
    public class UnitClass : ScriptableEnum
    {
        public int strength = 5;
        public int intelligence = 5;
        public int endurance = 5;
        public int dexterity = 5;
        [ReadOnly, ShowInInspector] public int PowerLevel => strength + intelligence + endurance + dexterity;
    }
}