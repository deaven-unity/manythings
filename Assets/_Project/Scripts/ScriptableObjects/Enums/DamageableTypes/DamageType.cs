﻿using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Enums.DamageableTypes
{
    [CreateAssetMenu(fileName = "DamageType", menuName = "Enums/DamageType", order = 0)]
    public class DamageType : ScriptableEnum
    {
        public DamageType strongAgainst;
        [Tooltip("Against effective damage type")]
        [Range(1, 5)] public float damageMultiplier;
    }
}