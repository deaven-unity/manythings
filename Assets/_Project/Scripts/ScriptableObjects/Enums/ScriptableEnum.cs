﻿using System;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Enums
{
    [CreateAssetMenu(fileName = "DefaultEnum", menuName = "Enums/DefaultEnum", order = 0)]
    public class ScriptableEnum : ScriptableObject
    {
        public new String name;
    }
}