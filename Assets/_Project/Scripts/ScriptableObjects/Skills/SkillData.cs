﻿using System;
using _Project.Scripts.ScriptableObjects.Enums.DamageableTypes;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Skills
{
    [CreateAssetMenu(fileName = "Skill", menuName = "Skills/Default", order = 0)]
    public class SkillData : ScriptableObject
    {
        public new String name;
        public Sprite icon;
        public GameObject prefab;
        public float cooldown;
        public DamageType damageType;
        public int damage;
    }
}