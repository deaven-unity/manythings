﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "AudioClipCollection", menuName = "Audio/ClipCollection", order = 0)]
    public class AudioClipCollection : ScriptableObject
    {
        public List<AudioClip> collection;
    }
}