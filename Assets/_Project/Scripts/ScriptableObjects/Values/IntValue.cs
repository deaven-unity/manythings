﻿using System;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.Values
{
    [CreateAssetMenu(fileName = "IntValue", menuName = "Values/IntValue", order = 0)]
    public class IntValue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private int defaultValue;
        [NonSerialized] private int runtimeValue;

        public event Action<int> ValueChanged;

        public int RuntimeValue
        {
            get => runtimeValue;
            set
            {
                if (value == runtimeValue)
                    return;

                runtimeValue = value;
                ValueChanged?.Invoke(runtimeValue);
            }
        }
        
        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(runtimeValue);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            runtimeValue = defaultValue;
        }
    }
}