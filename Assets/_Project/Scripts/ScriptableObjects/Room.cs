using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Room", menuName = "Grid/Room", order = 0)]
    public class Room : ScriptableObject
    {
        public Texture2D texture;
        public GridBlock defaultBlock;
        public GridBlock[] blocks;
    }
}