﻿using System;
using _Project.Scripts.ScriptableObjects.Values;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Resource", menuName = "Resource", order = 0)]
    public class Resource : IntValue
    {
        public new String name;
        public Sprite icon;
        public int max = 100;

        public void Add(int value)
        {
            if (RuntimeValue + value > max)
            {
                Debug.Log($"{name} reached it's limit!");
                return;
            }

            RuntimeValue += value;
        }

        public void Subtract(int value)
        {
            if (RuntimeValue - value < 0)
            {
                Debug.Log($"{name} is empty!");
                return;
            }

            RuntimeValue -= value;
        }

        public void Clear()
        {
            RuntimeValue = 0;
        }
    }
    
#if UNITY_EDITOR
    [CustomEditor(typeof(Resource), true)]
    public class ResourceEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Resource t = target as Resource;

            if (GUILayout.Button("Add"))
                t.Add(10);
            if (GUILayout.Button("Subtract"))
                t.Subtract(10);
            if (GUILayout.Button("Clear"))
                t.Clear();
        }
    }
#endif
}