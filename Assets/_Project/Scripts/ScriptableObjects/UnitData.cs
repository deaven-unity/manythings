﻿using System;
using System.Collections.Generic;
using _Project.Scripts.ScriptableObjects.Enums.DamageableTypes;
using _Project.Scripts.ScriptableObjects.Enums.UnitClasses;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "UnitData", menuName = "Unit", order = 0)]
    public class UnitData : ScriptableObject
    {
        public new String name;
        public UnitClass unitClass;
        
        public int maxHealth = 100;
        public float invulnerableTime = .25f;
        
        public float viewRange = 3f;
        public int baseDamage;
        public DamageType weakness;
        
        public GameObject prefab;
        public List<ResourceCosts> resourceCosts;
    }
}