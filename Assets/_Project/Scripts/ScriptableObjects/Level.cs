using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Level", menuName = "Grid/Level", order = 0)]
    public class Level : ScriptableObject
    {
        public Texture2D texture;
        public GridBlock defaultBlock;
        public List<GridBlock> blocks = new List<GridBlock>();
    }
}