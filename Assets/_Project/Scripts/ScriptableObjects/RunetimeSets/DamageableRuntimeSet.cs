﻿using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.RunetimeSets
{
    [CreateAssetMenu(fileName = "DamageableRuntimeSet", menuName = "RuntimeSets/DamageableRuntimeSet", order = 0)]
    public class DamageableRuntimeSet : RuntimeSet<Damageable>
    {
    }
}