﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.RunetimeSets
{
    public abstract class RuntimeSet<T> : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<T> defaultValue = new List<T>();
        [NonSerialized, ShowInInspector] private List<T> _runtimeValues = new List<T>();
        
        public event Action<T> ValueChanged;

        public List<T> RuntimeValues => _runtimeValues;

        public virtual void Add(T t)
        {
            if (RuntimeValues.Contains(t))
                return;
            
            RuntimeValues.Add(t);
            ValueChanged?.Invoke(t);
        }

        public virtual void Remove(T t)
        {
            if (!RuntimeValues.Contains(t)) 
                return;
            
            RuntimeValues.Remove(t);
            ValueChanged?.Invoke(t);
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            _runtimeValues = defaultValue;
        }
    }
}