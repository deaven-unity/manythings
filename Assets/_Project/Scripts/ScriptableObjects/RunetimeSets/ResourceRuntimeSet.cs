﻿using UnityEngine;

namespace _Project.Scripts.ScriptableObjects.RunetimeSets
{
    [CreateAssetMenu(fileName = "ResourceRuntimeSet", menuName = "RuntimeSets/ResourceRuntimeSet", order = 0)]
    public class ResourceRuntimeSet : RuntimeSet<Resource>
    {
        
    }
}