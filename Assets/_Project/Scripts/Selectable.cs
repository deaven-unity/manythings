using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace _Project.Scripts
{
    public class Selectable : MonoBehaviour, ISelectable
    {
        [FoldoutGroup("Selection Events")]
        [SerializeField]private UnityEvent OnEnter;
        [FoldoutGroup("Selection Events")]
        [SerializeField]private UnityEvent OnExit;
        [FoldoutGroup("Selection Events")]
        [SerializeField] private UnityEvent OnClick;
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            OnEnter.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnExit.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick.Invoke();
        }
    }
}