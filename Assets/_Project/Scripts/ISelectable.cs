using UnityEngine.EventSystems;

namespace _Project.Scripts
{
    public interface ISelectable : IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        
    }
}