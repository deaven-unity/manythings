﻿using _Project.Scripts.ScriptableObjects.RunetimeSets;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UiUnitMonitor : MonoBehaviour
    {
        [SerializeField] private DamageableRuntimeSet dataSet;
        
        private TextMeshProUGUI _text;

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _text.SetText(dataSet.RuntimeValues.Count.ToString());
        }

        private void OnEnable()
        {
            dataSet.ValueChanged += UpdateText;
        }

        private void OnDisable()
        {
            dataSet.ValueChanged -= UpdateText;
        }

        private void UpdateText(Damageable damageable)
        {
            _text.SetText(dataSet.RuntimeValues.Count.ToString());
        }
    }
}