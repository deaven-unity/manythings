﻿using _Project.Scripts.ScriptableObjects;
using _Project.Scripts.ScriptableObjects.Items;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts
{
    public class Damagedealer : MonoBehaviour
    {
        public UnitData unitData;
        public Weapon weapon;
        [ReadOnly] public Damageable currentTarget;

        [FoldoutGroup("Events")]
        [SerializeField] private UnityEvent OnAttack;

        public void Attack()
        {
            Attack(currentTarget);
        }

        [Button]
        public void Attack(Damageable target)
        {
            int damage = unitData.baseDamage;

            if (weapon)
                damage += weapon.damage;
            
            bool isEffective = target.unitData.weakness == unitData.weakness.strongAgainst;

            if (isEffective)
                damage = (int) (damage * unitData.weakness.strongAgainst.damageMultiplier);
            
            OnAttack.Invoke();
            target.TakeDamage(damage);
        }
        
    }
}