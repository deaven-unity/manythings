﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts
{
    public class TriggerActionEvents : MonoBehaviour
    {
        public UnityEvent onTriggerEnterEvent;
        public UnityEvent onTriggerExitEvent;
        
        public void OnTriggerEnter(Collider other)
        {
            onTriggerEnterEvent.Invoke();
        }

        private void OnTriggerExit(Collider other)
        {
            onTriggerExitEvent.Invoke();
        }
    }
}