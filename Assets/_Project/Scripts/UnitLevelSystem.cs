﻿using _Project.Scripts.ScriptableObjects;
using _Project.Scripts.ScriptableObjects.Enums.UnitClasses;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts
{
    public class UnitLevelSystem : MonoBehaviour
    {
        [SerializeField] private UnitClass _unitClass;
        [SerializeField] private StatsMultiplicationTable _statsMultiplicationTable;
        
        private int _strength;
        private int _intelligence;
        private int _endurance;
        private int _dexterity;

        private void Start()
        {
            ResetToBaseStats();
        }

        [Button]
        public void ResetToBaseStats()
        {
            _strength = _unitClass.strength;
            _intelligence = _unitClass.intelligence;
            _endurance = _unitClass.endurance;
            _dexterity = _unitClass.dexterity;
        }
    }
}