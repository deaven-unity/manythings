﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

namespace _Project.Scripts
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Animator _anim;

        private InputActions _playerControls;
        private NavMeshAgent _agent;
        private int _animSpeedId;
        private int _animAttackId;
        private float _motionSmoothTime = .1f;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();

            _playerControls = new InputActions();

            _playerControls.Gameplay.Move.performed += OnMove;
            _playerControls.Gameplay.DefaultAttack.performed += OnDefaultAttack;
            
            _animSpeedId = Animator.StringToHash("moveSpeed");
            _animAttackId = Animator.StringToHash("defaultAttack");
        }

        private void OnEnable()
        {
            _playerControls.Enable();
            _playerControls.Gameplay.Enable();
        }

        private void Update()
        {
            float speed = _agent.velocity.magnitude / _agent.speed;
            
            _anim.SetFloat(_animSpeedId, speed, _motionSmoothTime, Time.deltaTime);
        }

        private void OnDisable()
        {
            _playerControls.Disable();
            _playerControls.Gameplay.Disable();
        }

        public void OnDefaultAttack(InputAction.CallbackContext context)
        {
            _anim.SetTrigger(_animAttackId);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            int layerMask = 1 << 0;
            
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (!Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask, QueryTriggerInteraction.Ignore))
                return;

            _agent.SetDestination(hit.point);

        }
    }
}