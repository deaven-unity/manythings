﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace _Project.Scripts
{
    public class ChangeVignette : MonoBehaviour
    {
        [SerializeField] private float duration;
        [SerializeField] private float intensity;

        private float _originalIntensity;
        private Volume _volume;
        private Vignette _vignette;

        private void Awake()
        {
            _volume = GetComponent<Volume>();
            _volume.profile.TryGet(out Vignette tmp);
            _vignette = tmp;
        }

        public void Apply()
        {
            if (!_vignette)
                return;
            
            _originalIntensity = _vignette.intensity.value;
            StartCoroutine(WaitForTime());
        }
        
        private IEnumerator WaitForTime()
        {
            Change();
            
            yield return new WaitForSeconds(duration);

            Reset();
        }

        private void Change()
        {
            _vignette.intensity.value = intensity;
        }

        private void Reset()
        {
            _vignette.intensity.value = _originalIntensity;
        }
    }
}