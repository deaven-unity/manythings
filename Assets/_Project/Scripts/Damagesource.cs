﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class Damagesource : MonoBehaviour
    {
        public int damage;
        public UnityEvent OnEnter;
        public UnityEvent OnExit;

        private Collider _collider;

        private void OnTriggerEnter(Collider other)
        {
            OnEnter.Invoke();
            
            if (!other.TryGetComponent(out Damageable damageable))
                return;
            
            damageable.TakeDamage(damage);
        }

        private void OnTriggerExit(Collider other)
        {
            OnExit.Invoke();
        }
    }
}