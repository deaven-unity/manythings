﻿using System;
using _Project.Scripts.ScriptableObjects;

namespace _Project.Scripts
{
    [Serializable]
    public class ResourceCosts
    {
        public Resource resource;
        public int value;
    }
}