﻿using System;
using System.Collections;
using _Project.Scripts.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts
{
    public class Damageable : MonoBehaviour
    {
        public UnitData unitData;

        [SerializeField] private bool invulnerable;
        [ShowInInspector, ReadOnly] private int _maxHealth;
        [ShowInInspector, ReadOnly] private int _health;
        [ShowInInspector, ReadOnly] private bool _isAlive;

        public event Action Damaged;
        public event Action Healed;
        public event Action Died;

        [FoldoutGroup("Events")]
        [SerializeField] private UnityEvent OnDamaged;
        [FoldoutGroup("Events")]
        [SerializeField] private UnityEvent OnHealed;
        [FoldoutGroup("Events")]
        [SerializeField] private UnityEvent OnDeath;

        public bool IsAlive => _isAlive;

        protected virtual int Health
        {
            get => _health;
            set
            {
                if (invulnerable)
                    return;
                
                _health = value;
                _isAlive = _health > 0;
                
                if (_health <= 0)
                {
                    Died?.Invoke();
                    OnDeath.Invoke();
                }
            }
        }

        protected virtual int MaxHealth
        {
            get => _maxHealth;
            set
            {
                _maxHealth = value;
                
                if (_health > _maxHealth)
                    _health = _maxHealth;
            }
        }

        private void Start()
        {
            MaxHealth = unitData.maxHealth;
            Health = MaxHealth;
        }

        [Button]
        [FoldoutGroup("Debug Actions")]
        public void IncreaseMaxHealth(int value)
        {
            MaxHealth += value;
            Health += value;
        }

        [Button]
        [FoldoutGroup("Debug Actions")]
        public void DecreaseMaxHealth(int value)
        {
            MaxHealth -= value;
        }

        [Button]
        [FoldoutGroup("Debug Actions")]
        public void ResetMaxHealth()
        {
            MaxHealth = unitData.maxHealth;
        }
        
        [Button]
        [FoldoutGroup("Debug Actions")]
        public void TakeDamage(int value)
        {
            Damaged?.Invoke();
            OnDamaged.Invoke();

            if (value >= Health)
            {
                Health = 0;
                return;
            }

            Health -= value;
            
            if (!invulnerable)
                StartCoroutine(InvulnerableTimer());
        }

        [Button]
        [FoldoutGroup("Debug Actions")]
        public void Heal(int value)
        {
            Healed?.Invoke();
            OnHealed.Invoke();

            if (value + Health >= MaxHealth)
            {
                Health = MaxHealth;
                return;
            }

            Health += value;
        }

        [Button]
        [FoldoutGroup("Debug Actions")]
        public void Kill()
        {
            TakeDamage(MaxHealth);
        }

        private IEnumerator InvulnerableTimer()
        {
            // if (invulnerable)
                // yield return 0; 
                    
            invulnerable = true;
            yield return new WaitForSeconds(unitData.invulnerableTime);
            invulnerable = false;
        }
    }
}