using _Project.Scripts.Character.Stats;
using UnityEngine;

namespace _Project.Scripts.Character
{
    [CreateAssetMenu(fileName = "CharacterClass", menuName = "CharacterClass", order = 0)]
    public class CharacterClass : ScriptableObject
    {
        public int damage = 10;
        public int defense = 10;
        public int maxHealth = 100;
        public int cautiousness = 10;
        
        public int strengthPerLevel = 2;
        public int dexterityPerLevel = 2;
        public int intelligencePerLevel = 2;
        public int endurancePerLevel = 2;
        
        public CharacterStats stats = new CharacterStats();
    }
}