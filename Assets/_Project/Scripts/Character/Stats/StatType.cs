namespace _Project.Scripts.Character.Stats
{
    public enum StatType
    {
        Strength,
        Dexterity,
        Intelligence,
        Endurance
    }
}