using System;
using UnityEngine;

namespace _Project.Scripts.Character.Stats
{
    [Serializable]
    public struct StatValue : IEquatable<StatValue>
    {
        [HideInInspector]
        public StatType Type;
        public int Value;

        public StatValue(StatType type, int value)
        {
            Type = type;
            Value = value;
        }

        public StatValue(StatType type)
        {
            Type = type;
            Value = 0;
        }

        public bool Equals(StatValue other)
        {
            return Type == other.Type;
        }
    }
}