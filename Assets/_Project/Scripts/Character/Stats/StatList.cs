using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Character.Stats
{
    [Serializable]
    public class StatList
    {
        [SerializeField]
        private List<StatValue> _stats = new List<StatValue>();
        
        public int Count
        {
            get { return _stats.Count; }
        }
        
        public StatValue this[int index]
        {
            get { return _stats[index]; }
            set { _stats[index] = value; }
        }
        
        public int this[StatType type]
        {
            get
            {
                for (int i = 0; i < _stats.Count; i++)
                {
                    if (_stats[i].Type == type)
                    {
                        return _stats[i].Value;
                    }
                }

                return 0;
            }
            set
            {
                for (int i = 0; i < _stats.Count; i++)
                {
                    if (_stats[i].Type == type)
                    {
                        var val = _stats[i];
                        val.Value = value;
                        _stats[i] = val;
                        return;
                    }
                }

                _stats.Add(new StatValue(type, value));
            }
        }
    }
}