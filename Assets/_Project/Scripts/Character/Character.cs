using System;
using _Project.Scripts.Character.Stats;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Character
{
    [Serializable]
    public class Character : MonoBehaviour
    {
        [SerializeField] [OnValueChanged("Init")]
        private CharacterClass defaultClass;

        [SerializeField, InlineEditor, ReadOnly]
        private CharacterClass runtimeClass;

        private int _level = 1;

        [ShowInInspector, ReadOnly]
        public int Damage
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.damage;
                int runtimeValue = runtimeClass.stats.Strength * 5;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public int HaxHealth
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.maxHealth;
                int runtimeValue = runtimeClass.stats.Endurance * 10;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public int Defense
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.defense;
                int runtimeValue = runtimeClass.stats.Endurance * 5;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public int Cautiousness
        {
            get
            {
                if (runtimeClass == null)
                    return 0;

                int defaultValue = runtimeClass.cautiousness;
                int runtimeValue = runtimeClass.stats.Intelligence * 5;

                int value = defaultValue + runtimeValue;

                return value;
            }
        }

        [ShowInInspector, ReadOnly]
        public int Level
        {
            get => _level;
            set => _level = value;
        }

        private void OnEnable()
        {
            Init();
        }

        [Button]
        public void Init()
        {
            Level = 1;

            runtimeClass = ScriptableObject.CreateInstance<CharacterClass>();
            runtimeClass.damage = defaultClass.damage;
            runtimeClass.defense = defaultClass.defense;
            runtimeClass.maxHealth = defaultClass.maxHealth;
            runtimeClass.cautiousness = defaultClass.cautiousness;

            runtimeClass.strengthPerLevel = defaultClass.strengthPerLevel;
            runtimeClass.dexterityPerLevel = defaultClass.dexterityPerLevel;
            runtimeClass.intelligencePerLevel = defaultClass.intelligencePerLevel;
            runtimeClass.endurancePerLevel = defaultClass.endurancePerLevel;

            runtimeClass.stats = new CharacterStats(defaultClass.stats);
        }

        [Button]
        public void LevelUp()
        {
            Level++;
            
            runtimeClass.stats.Strength += runtimeClass.strengthPerLevel;
            runtimeClass.stats.Dexterity += runtimeClass.dexterityPerLevel;
            runtimeClass.stats.Intelligence += runtimeClass.intelligencePerLevel;
            runtimeClass.stats.Endurance += runtimeClass.endurancePerLevel;
        }

        [Button]
        public void SetLevel(int value)
        {
            Level = value <= 1 ? 1 : value;
            
            runtimeClass.stats.Strength = (Level - 1) * runtimeClass.strengthPerLevel + defaultClass.stats.Strength;
            runtimeClass.stats.Dexterity = (Level - 1) * runtimeClass.dexterityPerLevel + defaultClass.stats.Dexterity;
            runtimeClass.stats.Intelligence =
                (Level - 1) * runtimeClass.intelligencePerLevel + defaultClass.stats.Intelligence;
            runtimeClass.stats.Endurance = (Level - 1) * runtimeClass.endurancePerLevel + defaultClass.stats.Endurance;
        }

        [Button]
        public void LevelDown()
        {
            if (Level - 1 < 1)
                return;

            Level--;
            
            runtimeClass.stats.Strength -= runtimeClass.strengthPerLevel;
            runtimeClass.stats.Dexterity -= runtimeClass.dexterityPerLevel;
            runtimeClass.stats.Intelligence -= runtimeClass.intelligencePerLevel;
            runtimeClass.stats.Endurance -= runtimeClass.endurancePerLevel;
        }
    }
}