﻿using _Project.Scripts.ScriptableObjects;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayAudioCollection : MonoBehaviour
    {
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        public void PlayRandomOneShot(AudioClipCollection collection)
        {
            int i = Random.Range(0, collection.collection.Count);
            
            _audioSource.PlayOneShot(collection.collection[i]);
        }
    }
}