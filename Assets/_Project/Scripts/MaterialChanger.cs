using UnityEngine;

namespace _Project.Scripts
{
    public class MaterialChanger : MonoBehaviour
    {
        [SerializeField] private Material first;
        [SerializeField] private Material second;

        private Renderer _renderer;

        private void Start()
        {
            _renderer = GetComponent<Renderer>();
        }

        public void Apply()
        {
            _renderer.materials = new[] {first, second};
        }

        public void Remove()
        {
            _renderer.materials = new[] {first};
        }

    }
}