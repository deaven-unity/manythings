using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

namespace _Project.Scripts
{
    public class Agent : MonoBehaviour
    {
        private NavMeshAgent _agent;
        private InputActions _playerControls;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            
            _playerControls = new InputActions();

            _playerControls.Gameplay.Move.performed += OnMove;
        }
        
        private void OnEnable()
        {
            _playerControls.Enable();
            _playerControls.Gameplay.Enable();
        }
        
        private void OnDisable()
        {
            _playerControls.Disable();
            _playerControls.Gameplay.Disable();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            // int layerMask = 1 << 0;
            
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (!Physics.Raycast(ray, out hit, Mathf.Infinity))
                return;

            _agent.SetDestination(hit.point);

        }
    }
}