using System.Collections.Generic;
using _Project.Scripts.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Grid
{
    public class RoomSpawner : MonoBehaviour
    {
        public Room[] rooms;
        private Vector3 _spawnPointer;
        private List<GameObject> _spawnedRooms = new List<GameObject>();

        private void Start()
        {
            Spawn();
        }

        [Button]
        public void Spawn()
        {
            _spawnPointer = Vector3.zero;

            for (int i = 0; i < rooms.Length; i++)
            {
                GameObject tmpRoom = new GameObject();
                tmpRoom.AddComponent<BoxCollider>();
                tmpRoom.transform.parent = transform;
                tmpRoom.name = rooms[i].texture.name;

                SpawnBlocks(rooms[i], tmpRoom.transform);
                
                tmpRoom.transform.position = _spawnPointer;

                Vector3 dir = GetRandomDirection();

                _spawnPointer += new Vector3(rooms[i].texture.width*3 * dir.x, 0, rooms[i].texture.height*3 * dir.z);
                
                _spawnedRooms.Add(tmpRoom.gameObject);
            }
        }

        private bool CheckSpawnLocation()
        {
            return true;
        }

        private Vector3 GetRandomDirection()
        {
            int random = Random.Range(0, 4);
            Vector3 dir = Vector3.zero;

            switch (random)
            {
                case 0: // UP
                    dir = new Vector3(0, 0, 1);
                    break;
                case 1: // DOWN
                    dir = new Vector3(0, 0, -1);
                    break;
                case 2: // RIGHT
                    dir = new Vector3(1, 0, 0);
                    break;
                case 3: // LEFT
                    dir = new Vector3(-1, 0, 0);
                    break;
            }

            return dir;
        }

        private void SpawnBlocks(Room room, Transform parent)
        {
            for (var y = 0; y < room.texture.height; y++)
            {
                for (int x = 0; x < room.texture.width; x++)
                {
                    Color color = room.texture.GetPixel(x, y);

                    GridBlock block = GetBlock(color, room);
                    Vector3 size = block.prefab.transform.localScale;

                    GameObject tmp = Instantiate(block.prefab,
                        new Vector3(x * size.x, size.y * block.heightLevel, y * size.z), Quaternion.identity,
                        parent);
                        
                    if (block.destroyable)
                    {
                        for (int i = block.heightLevel - 1; i >= 0; i--)
                        {
                            GameObject tmp2 = Instantiate(room.defaultBlock.prefab,
                                new Vector3(x * size.x, size.y * i, y * size.z),
                                Quaternion.identity, parent);
                        }
                    }

                    tmp.name = $"{tmp.name}: {x} : {y}";
                }
            }
        }
        
        private GridBlock GetBlock(Color color, Room room)
        {
            foreach (GridBlock block in room.blocks)
            {
                if (ColorEqual(block.color, color))
                    return block;
            }
        
            return room.defaultBlock;
        }
        
        [Button]
        public void RemoveChildren()
        {
            var tempArray = new GameObject[transform.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = transform.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                DestroyImmediate(child);
            }
        }
        
        private bool ColorEqual(Color color1, Color color2)
        {
            float threshold = 0.1f;
            return (Mathf.Abs(color1.r - color2.r) < threshold
                    && Mathf.Abs(color1.g - color2.g) < threshold
                    && Mathf.Abs(color1.b - color2.b) < threshold);
        }

    }
}