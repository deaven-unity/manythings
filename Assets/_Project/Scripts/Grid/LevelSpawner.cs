using System.Collections.Generic;
using _Project.Scripts.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Grid
{
    public class LevelSpawner : MonoBehaviour
    {
        public Level[] level;
        private List<GameObject> _nodes = new List<GameObject>();

        [Button]
        public void SpawnRandom()
        {
            int random = Random.Range(0, level.Length);
            
            Spawn(level[random]);
        }
        
        [Button]
        public void Spawn(Level level)
        {
            RemoveChildren();
            
            for (var y = 0; y < level.texture.height; y++)
            {
                for (int x = 0; x < level.texture.width; x++)
                {
                    Color color = level.texture.GetPixel(x, y);

                    GridBlock block = GetBlock(color, level);
                    Vector3 size = block.prefab.transform.localScale;

                    GameObject tmp = Instantiate(block.prefab,
                        new Vector3(x * size.x, size.y * block.heightLevel, y * size.z), Quaternion.identity,
                        transform);
                    
                    if (block.destroyable)
                    {
                        for (int i = block.heightLevel - 1; i >= 0; i--)
                        {
                            GameObject tmp2 = Instantiate(level.defaultBlock.prefab,
                                new Vector3(x * size.x, size.y * i, y * size.z),
                                Quaternion.identity, transform);
                            _nodes.Add(tmp2);
                        }
                    }

                    tmp.name = $"{tmp.name}: {x} : {y}";
                    _nodes.Add(tmp);
                }
            }
        }
        
        [Button]
        public void RemoveChildren()
        {
            var tempArray = new GameObject[transform.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = transform.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                DestroyImmediate(child);
            }
        }

        private GridBlock GetBlock(Color color, Level level)
        {
            foreach (GridBlock block in level.blocks)
            {
                if (ColorEqual(block.color, color))
                    return block;
            }

            return level.defaultBlock;
        }
        
        private bool ColorEqual(Color color1, Color color2)
        {
            float threshold = 0.1f;
            return (Mathf.Abs(color1.r - color2.r) < threshold
                    && Mathf.Abs(color1.g - color2.g) < threshold
                    && Mathf.Abs(color1.b - color2.b) < threshold);
        }
    }
}