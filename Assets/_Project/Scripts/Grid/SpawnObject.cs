using UnityEngine;

namespace _Project.Scripts.Grid
{
    public class SpawnObject : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] tiles;
    
        void Start()
        {
            int random = Random.Range(0, tiles.Length);
            Instantiate(tiles[random], transform.position, Quaternion.identity, transform.parent);
        }
    }
}
