namespace _Project.Scripts.Grid
{
    public class Grid<T> where T : Block, new()
    {
        public int height;
        public int width;


        public T[] GenerateGrid(int height, int width)
        {
            T[] tmp = new T[height * width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    tmp[i] = new T();
                }
            }

            return tmp;

        }
    }
}