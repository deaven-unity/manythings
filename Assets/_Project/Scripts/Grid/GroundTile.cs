using UnityEngine;

namespace _Project.Scripts.Grid
{
    public class GroundTile : MonoBehaviour
    {
        // public UnityEvent OnMouseEnter();

        private void OnMouseEnter()
        {
            Debug.Log($"Mouse Enter {gameObject.name}");
        }

        private void OnMouseExit()
        {
            Debug.Log($"Mouse Exit {gameObject.name}");
        }

        private void OnMouseOver()
        {
            Debug.Log($"Mouse Over {gameObject.name}");
        }
    }
}