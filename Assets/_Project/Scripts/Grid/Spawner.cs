﻿using _Project.Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Grid
{
    public class Spawner : MonoBehaviour
    {
        public UnitData data;
        public Transform spawnPoint;

        public void Spawn()
        {
            Instantiate(data.prefab, spawnPoint.position, Quaternion.identity);
        }
    }
    
    #if UNITY_EDITOR
    
    [CustomEditor(typeof(Spawner), true)]
    public class SpawnerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Spawner t = target as Spawner;

            if (GUILayout.Button("Spawn"))
                t.Spawn();
        }
    }
    
    #endif
}