using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Grid
{
    public class GridGenerator : MonoBehaviour
    {
        public int xSize = 10;
        public int zSize = 10;
        public float spacing = 0f;
        public GameObject groundPrefab;
        public GameObject dirtPrefab;

        [MinMaxSlider(0, 2)] public Vector2 perlinScale = new Vector2(1, 1);
        [MinMaxSlider(0, 2)] public Vector2 perlinScaleX = new Vector2(1, 1);
        [MinMaxSlider(0, 2)] public Vector2 perlinScaleZ = new Vector2(1, 1);

        private float _perlinScale;
        private float _perlinScaleX;
        private float _perlinScaleZ;

        private GameObject _ground;
        private GameObject _dirt;
        private NavMeshSurface _navMeshSurface;

        private void Awake()
        {
            _navMeshSurface = GetComponent<NavMeshSurface>();
        }

        [Button]
        public void Generate()
        {
            RemoveChildren(transform);

            GenerateGround();
            GenerateDirt();

            BakeNavMesh();
        }

        public void BakeNavMesh()
        {
            if (!_navMeshSurface)
                _navMeshSurface = GetComponent<NavMeshSurface>();

            _navMeshSurface.BuildNavMesh();
        }

        public void GenerateDirt()
        {
            List<GameObject> dirt = new List<GameObject>();

            _dirt = new GameObject();
            _dirt.name = "Dirt";
            _dirt.transform.parent = transform;

            dirt = GenerateGrid(dirtPrefab, xSize, zSize, spacing, _dirt.transform, 10, true);

            float prefabHeight = dirt[0].transform.localScale.y / 2;

            foreach (GameObject d in dirt)
            {
                Ray ray = new Ray(d.transform.position, Vector3.down);

                Debug.DrawRay(d.transform.position, Vector3.down, Color.blue);

                if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
                    continue;

                d.transform.position = hit.point + new Vector3(0, prefabHeight, 0);
            }
        }

        public void GenerateGround()
        {
            _ground = new GameObject();
            _ground.name = "Ground";
            _ground.transform.parent = transform;
            GenerateGrid(groundPrefab, xSize, zSize, spacing, _ground.transform);
        }

        [Button]
        public void RemoveAll()
        {
            RemoveChildren(transform);
            BakeNavMesh();
        }

        [Button]
        public void RemoveGround()
        {
            RemoveChildren(_ground.transform);
            BakeNavMesh();
        }

        [Button]
        public void RemoveDirt()
        {
            RemoveChildren(_dirt.transform);
        }

        private void RemoveChildren(Transform t)
        {
            var tempArray = new GameObject[t.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = t.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                DestroyImmediate(child);
            }
        }

        private List<GameObject> GenerateGrid(GameObject prefab, int _xSize, int _zSize, float _spacing,
            Transform parent, float yOffset = 0f, bool usePerlin = false)
        {
            List<GameObject> list = new List<GameObject>();


            for (int x = 0; x < _xSize; x++)
            {
                for (int z = 0; z < _zSize; z++)
                {
                    if (usePerlin)
                    {
                        if (GetPerlinNoise(x, z) > .5)
                            continue;
                    }

                    var xPos = x + (_spacing * x);
                    var yPos = yOffset;
                    var zPos = z + (_spacing * z);

                    GameObject obj = Instantiate(prefab, new Vector3(xPos, yPos, zPos),
                        Quaternion.identity,
                        parent);

                    obj.name = x + " : " + z;

                    list.Add(obj);
                }
            }

            return list;
        }

        private float GetPerlinNoise(float x, float y)
        {
            float perlin;
            _perlinScale = Random.Range(perlinScale.x, perlinScale.y);
            _perlinScaleX = Random.Range(perlinScaleX.x, perlinScaleX.y);
            _perlinScaleZ = Random.Range(perlinScaleZ.x, perlinScaleZ.y);

            perlin = Mathf.PerlinNoise(x * _perlinScaleX, y * _perlinScaleZ) * _perlinScale;

            Mathf.Clamp(perlin, 0, 1);

            return perlin;
        }
    }
}