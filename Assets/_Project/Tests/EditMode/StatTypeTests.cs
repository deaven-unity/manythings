using System;
using _Project.Scripts.Character.Stats;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class StatTypeTests
    {
        [Test]
        public void StrengthTypeExists()
        {
            Assert.AreEqual(Enum.IsDefined(typeof(StatType), "Strength"), true);
        }
        
        [Test]
        public void DexterityTypeExists()
        {
            Assert.AreEqual(Enum.IsDefined(typeof(StatType), "Dexterity"), true);
        }
        
        [Test]
        public void IntelligenceTypeExists()
        {
            Assert.AreEqual(Enum.IsDefined(typeof(StatType), "Intelligence"), true);
        }
        
        [Test]
        public void EnduranceTypeExists()
        {
            Assert.AreEqual(Enum.IsDefined(typeof(StatType), "Endurance"), true);
        }
    }
}