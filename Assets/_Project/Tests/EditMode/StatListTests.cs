using _Project.Scripts.Character.Stats;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class StatListTests
    {
        [Test]
        public void CreateEmptyStatList()
        {
            StatList statList = new StatList();

            Assert.AreEqual(statList.Count, 0);
        }
        
        [Test]
        public void AccessingNoneExistingTypeCreatesNewOne()
        {
            StatList statList = new StatList();
            
            statList[StatType.Strength] = 10;
            Assert.AreEqual(statList.Count, 1);
        }
        
        [Test]
        public void SetValueOfStatType()
        {
            StatList statList = new StatList();
            
            statList[StatType.Strength] = 10;
            Assert.AreEqual(statList[StatType.Strength], 10);
        }
    }
}