using _Project.Scripts.Character.Stats;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class StatValueTests
    {
        [Test]
        public void CreateStrengthStatValueWithoutValue()
        {
            Scripts.Character.Stats.StatValue statValue = new Scripts.Character.Stats.StatValue(StatType.Strength);
            
            Assert.AreEqual(statValue.Type, StatType.Strength);
        }
        
        [Test]
        public void CreateStrengthStatValueWithValue()
        {
            Scripts.Character.Stats.StatValue statValue = new Scripts.Character.Stats.StatValue(StatType.Strength, 10);
            
            Assert.AreEqual(statValue.Type, StatType.Strength);
        }

        [Test]
        public void CompareTwoSameTypeStatValues()
        {
            Scripts.Character.Stats.StatValue statValue1 = new Scripts.Character.Stats.StatValue(StatType.Strength, 10);
            Scripts.Character.Stats.StatValue statValue2 = new Scripts.Character.Stats.StatValue(StatType.Strength, 15);
            
            Assert.AreEqual(statValue1.Equals(statValue2), true);
        }
        
        [Test]
        public void CompareTwoDifferentTypeStatValues()
        {
            Scripts.Character.Stats.StatValue statValue1 = new Scripts.Character.Stats.StatValue(StatType.Strength, 10);
            Scripts.Character.Stats.StatValue statValue2 = new Scripts.Character.Stats.StatValue(StatType.Intelligence, 15);
            
            Assert.AreEqual(statValue1.Equals(statValue2), false);
        }
    }
}